"use strict";

import { readFile } from 'fs/promises';
import { X509Certificate, createPrivateKey, createPublicKey } from 'crypto';

import { strict as assert } from 'assert';

// Define "require"
import { createRequire } from "module";
const require = createRequire(import.meta.url);


let x509CertFilename = './compliance.gaia-x.eu/compliance.gaia-x.eu.cer';
let x509 = new X509Certificate(await readFile(x509CertFilename, 'utf8'));
let x509privkey = createPrivateKey(await readFile('./compliance.gaia-x.eu/compliance.gaia-x.eu.key'));
console.log("Certificate: ", x509);
console.log("Private Key:", x509privkey);
assert.ok(x509.checkPrivateKey(x509privkey));

{
  const { createSign, createVerify } = await import('crypto');
  const privateKey = x509privkey, publicKey = x509.publicKey;
  const sign = createSign('SHA256');
  sign.update('some data to sign');
  sign.end();
  const signature = sign.sign(privateKey);

  const verify = createVerify('SHA256');
  verify.update('some data to sign');
  verify.end();
  assert.ok(verify.verify(publicKey, signature));
}

let vcprivkey = createPrivateKey(await readFile('./vc-ed25519-priv.pem'));
let vcpubkey = createPublicKey(vcprivkey);

{
  const { sign, verify } = await import('crypto');
  const privateKey = vcprivkey, publicKey = vcpubkey;
  const message = 'Hello world!';
  const signature = sign(null, Buffer.from(message), privateKey);
  assert.ok(verify(null, Buffer.from(message), publicKey, signature));
}

import vc from '@digitalbazaar/vc';
const { Ed25519VerificationKey2020 } = require('@digitalbazaar/ed25519-verification-key-2020');
const { Ed25519Signature2020 } = require('@digitalbazaar/ed25519-signature-2020');


{
  // from https://github.com/digitalbazaar/ed25519-verification-key-2020/blob/v3.2.0/lib/Ed25519VerificationKey2020.js

  const base58btc = require('base58-universal');

  // let base58btc2 = {base58btc};
  // multibase base58-btc header
  const MULTIBASE_BASE58BTC_HEADER = 'z';
  // multicodec ed25519-pub header as varint
  const MULTICODEC_ED25519_PUB_HEADER = new Uint8Array([0xed, 0x01]);
  // multicodec ed25519-priv header as varint
  const MULTICODEC_ED25519_PRIV_HEADER = new Uint8Array([0x80, 0x26]);

  // encode a multibase base58-btc multicodec key
  function encodeMbKey(header, key) {
    const mbKey = new Uint8Array(header.length + key.length);

    mbKey.set(header);
    mbKey.set(key, header.length);

    return MULTIBASE_BASE58BTC_HEADER + base58btc.encode(mbKey);
  }

  {
    const keyPair = await Ed25519VerificationKey2020.generate();
    console.log("Generated:", keyPair);
  }

  // const keyPair = await Ed25519VerificationKey2020.from({
  //     publicKeyMultibase: _encodeMbKey(MULTICODEC_ED25519_PUB_HEADER, keyObject.publicKey),
  //     privateKeyMultibase: _encodeMbKey(MULTICODEC_ED25519_PRIV_HEADER, keyObject.secretKey)
  // });

  let vcpubbuff = vcpubkey.export({ format: 'pem', type: 'spki' })
  vcpubbuff = vcpubbuff.replace(/-----(BEGIN|END) PUBLIC KEY-----/gm, "").trim();
  vcpubbuff = Buffer.from(vcpubbuff, 'base64');

  let vcprivbuff = vcprivkey.export({ format: 'pem', type: 'pkcs8' })
  vcprivbuff = vcprivbuff.replace(/-----(BEGIN|END) PRIVATE KEY-----/gm, "").trim();
  vcprivbuff = Buffer.from(vcprivbuff, 'base64');

  const keyPair = await Ed25519VerificationKey2020.from({
    publicKeyMultibase: encodeMbKey(MULTICODEC_ED25519_PUB_HEADER, vcpubbuff),
    privateKeyMultibase: encodeMbKey(MULTICODEC_ED25519_PRIV_HEADER, vcprivbuff)
  });

  console.log("Loaded:", keyPair);

  const suite = new Ed25519Signature2020({ key: keyPair });

  const axios = require('axios');
  let jsigs = require('jsonld-signatures');
  const {purposes: {AssertionProofPurpose}} = jsigs;

  let myDocumentLoader = async (url, options) => {
    console.log(url, options);
    let doc = await axios.get(url);
    return {contextUrl: null, document: doc.data, documentUrl: url};
    // return jsonld.documentLoader(doc.data);
};

  // Sample unsigned credential
  const credential = {
    "@context": [
      "https://www.w3.org/2018/credentials/v1",
      "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "id": "https://example.com/credentials/1872",
    "type": ["VerifiableCredential", "AlumniCredential"],
    "issuer": "https://example.edu/issuers/565049",
    "issuanceDate": "2010-01-01T19:23:24Z",
    "credentialSubject": {
      "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
      "alumniOf": "Example University"
    }
  };

  {
    let signed = await jsigs.sign(credential, {
      suite: suite,
      purpose: new AssertionProofPurpose(),
      documentLoader: myDocumentLoader
    });
    console.log(signed);

  }


  // const signedVC = await vc.issue({ credential, suite });
  // console.log(JSON.stringify(signedVC, null, 2));

  {
    let vcpubjwk = vcpubkey.export({ format: 'jwk', type: 'spki' });
    let vcprivjwk = vcprivkey.export({ format: 'jwk', type: 'pkcs8' });
    console.log(vcprivjwk)

    const DIDKit = require('@spruceid/didkit');
    console.log(DIDKit.getVersion());

    // const key = DIDKit.generateEd25519Key();
    const key = vcprivjwk;
    console.log(key);

    // There are two helpful functions to obtain a DID and the `did:key`
    // `verificationMethod` from the key.
    const did = DIDKit.keyToDID('key', vcprivjwk);
    console.log("DID:", did);
    const verificationMethod = DIDKit.keyToVerificationMethod('key', vcprivjwk);

    console.log("Verification Method:", verificationMethod);


    {
      const suite = new Ed25519Signature2020({ key: keyPair });

      console.log(suite);


      //   const signedVC = await vc.issue({credential, suite, documentLoader});

      // https://dpjanes.medium.com/vaccination-passport-encoded-as-a-w3c-verifiable-credential-3bb4a1b80e3e

      let member_email = "pierre.gronlier@gaia-x.eu";

      let claims = {
        "@context": {
          "schema:": "https://schema.org"
        },
        "@type": "schema:ProgramMembership",
        "hostingOrganization": {
          "@type": "schema:Organization",
          "schema:legalName": "GAIA-X European Association for Data and Cloud",
          "schema:identifier": "0762747721", // https://opencorporates.com/companies/be/0762747721 TODO use EUID
          "schema:location": {
            "@type": "PostalAddress",
            "schema:addressCountry": "BE"
          },
          "schema:logo": "https://...."
        },
        "schema:identifier": member_email,
        "schema:location": {
          "@context": {
            "schema:": "https://schema.org"
          },
          "@type": "schema:Hospital",
          "schema:name": "General Hospital",
          "schema:identifier": "MSC-PLC-0200-1001-2101",
          "schema:address": {
            "@type": "schema:PostalAddress",
            "schema:addressCountry": "CA",
            "schema:addressRegion": "BC"
          }
        }
      }

      // https://extendsclass.com/json-schema-validator.html
      // https://www.jsonschemavalidator.net/
      // https://json-ld.org/playground/
      // check how to use json-signatures https://github.com/digitalbazaar/jsonld-signatures/tree/v2.3.1

      {
        let data = {
          "@context": {
            "schema:": "https://schema.org"
          },
          "@type": "ProgramMembership",
          "schema:hostingOrganization": {
            "@type": "schema:Organization",
            "schema:legalName": "GAIA-X European Association for Data and Cloud",
            "schema:identifier": "0762747721",
            "schema:location": {
              "@type": "PostalAddress",
              "schema:addressCountry": "BE"
            },
            "schema:logo": "https://...."
          },
          "schema:identifier": "hash(member_email)",
          "schema:location": {
            "@context": {
              "schema2:": "https://schema.org"
            },
            "@type": "schema:Hospital",
            "schema:name": "General Hospital",
            "schema:identifier": "MSC-PLC-0200-1001-2101",
            "schema:address": {
              "@type": "schema:PostalAddress",
              "schema:addressCountry": "CA",
              "schema:addressRegion": "BC"
            }
          }
        }

      }

      let subject = {
        "id": "did:example:d23dd687a7dc6787646f2eb98d0"
      };
      const vcred = DIDKit.issueCredential({
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
        ],
        "id": "http://example.org/credentials/3731",
        "type": ["VerifiableCredential"],
        "issuer": did,
        "issuanceDate": "2020-08-19T21:41:50Z",
        "credentialSubject": subject
      }, {
        "proofPurpose": "assertionMethod",
        "verificationMethod": verificationMethod
      }, vcprivjwk);

      console.log(vcred);
      {
        const jose = require('jose');
        const ecPublicKey = await jose.importJWK(vcprivjwk, 'ES256')
        console.log(vcprivjwk, ecPublicKey);

        const jwt = await new jose.SignJWT(subject)
          .setProtectedHeader({ alg: 'EdDSA' })
          .setIssuedAt()
          .setIssuer('urn:example:issuer')
          .setAudience('urn:example:audience')
          .setExpirationTime('2h')
          .sign(vcprivkey)

        console.log(jwt)

      }
      member_email = "pierre.gronlier@gaia-x.eu";
      // Sample unsigned credential
      const credential = {
        "@context": {
          "vc": "https://www.w3.org/2018/credentials/v1",
          "schema:": "https://schema.org"
        },
        "vc:id": member_email,
        "vc:type": ["vc:VerifiableCredential", "schema:ProgramMembership"],
        "vc:issuer": did,
        "vc:issuanceDate": "2010-01-01T19:23:24Z",
        "vc:credentialSubject": {
          "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
          "alumniOf": "Example University"
        }
      };
      const signedVC = await vc.issue({ credential, suite });
      console.log(JSON.stringify(signedVC, null, 2));

    }
  }
}



// import { Ed25519VerificationKey2020 } from '@digitalbazaar/ed25519-verification-key-2020';
// const Ed25519VerificationKey2020 = require('@digitalbazaar/ed25519-verification-key-2020');

// import pkg from '@digitalbazaar/ed25519-verification-key-2020';
// const { Ed25519VerificationKey2020 } = pkg;

// console.log(Ed25519VerificationKey2020);

// const edKeyPair = await Ed25519VerificationKey2020.generate();

// // import Ed25519Signature2020 from '@digitalbazaar/ed25519-signature-2020';

// console.log(edKeyPair);
// // const serializedKeyPair = { ... };

// // const keyPair = await Ed25519VerificationKey2020.from(serializedKeyPair);

