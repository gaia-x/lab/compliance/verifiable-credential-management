import { strict as assert } from 'assert';
import { readFile } from 'fs/promises';
import { X509Certificate, createPrivateKey, createPublicKey } from 'crypto';

let config = {
    server: {
        port: process.env.PORT || 3000
    },
    cert: {
        domain: {
            certificateFile: process.env.DOMAIN_CERTIFICATE_FILE || './vc.gaia-x.eu/fullchain.cer',
            keyFile: process.env.DOMAIN_CERTIFICATE_KEY_FILE || './vc.gaia-x.eu/vc.gaia-x.eu.key'
        },
        issuer: {
            privateKeyFile: process.env.VC_PRIVKEY || './vc-ed25519-priv.pem'
        }
    }
}

config.cert.domain.certificate = new X509Certificate(await readFile(config.cert.domain.certificateFile, 'utf8'));
config.cert.domain.key = createPrivateKey(await readFile(config.cert.domain.keyFile));

assert.ok(config.cert.domain.certificate.checkPrivateKey(config.cert.domain.key));

config.cert.issuer.privateKey = createPrivateKey(await readFile(config.cert.issuer.privateKeyFile));
config.cert.issuer.publicKey = createPublicKey(config.cert.issuer.privateKey);

export default config;
