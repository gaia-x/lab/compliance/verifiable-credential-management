"use strict";
import { strict as assert } from 'assert';
import { verifyWithDer, verifyWithJwk, convertToMultibase } from "./utils.js";
import { getController } from './did.js';
import { Resolver } from 'did-resolver'
import { getResolver } from 'web-did-resolver'

// Define "require"
import { createRequire } from "module";
const require = createRequire(import.meta.url);

const jsonld = require('jsonld');
const jose = require('jose');
const axios = require('axios');
const jp = require('jsonpath');

let jsigs = require('jsonld-signatures');
const { purposes: { AssertionProofPurpose } } = jsigs;
import vc from '@digitalbazaar/vc';
// import { JsonWebKey, JsonWebSignature } from "@transmute/json-web-signature";
import jsonWebSignature2020Pkg from "@transmute/json-web-signature-2020";
const { JsonWebKey, JsonWebSignature } = jsonWebSignature2020Pkg;

let { Ed25519VerificationKey2020 } = require('@digitalbazaar/ed25519-verification-key-2020');
let { Ed25519Signature2020 } = require('@digitalbazaar/ed25519-signature-2020');

const didResolver = new Resolver({
    ...getResolver()
    //...you can flatten multiple resolver methods into the Resolver
})

export async function myDocumentLoader(url, options) {
    const doc = await docResolver(url, options);
    if (url.includes('#')) {
        let results = jp.query(doc, `$..[?(@.id=="${url}")]`);
        if (results.length == 0) {
            throw `unresolved entry ${url} in retrieved document`;
        }
        return { contextUrl: null, document: results[0], documentUrl: url };
    } else {
        return { contextUrl: null, document: doc, documentUrl: url };
    }
};

async function docResolver(url, options) {
    console.log("resolving", url, options);
    if (url.startsWith('did:web:')) {
        return (await didResolver.resolve(url)).didDocument;
    }
    if (url.startsWith('http')) {
        const doc = await axios.get(url);
        if (doc.status >= 400) {
            throw `unresolved url ${url}`;
        }
        return doc.data;
    }
    throw `unknown resolver for schema ${url}`;
};

async function jws_sign(options) {
    // const claim = {...options.claim};
    const claim = options.claim;
    const jwt = await new jose.SignJWT(claim)
    .setProtectedHeader({ alg: 'RS256' })
    .sign(options.privateKey);
    const jwt_elts = jwt.split('.');

    // from https://w3c-ccg.github.io/lds-jws2020/#example-8
    let createdDate = (new Date()).toISOString();
    createdDate = createdDate.substr(0, createdDate.length - 5) + 'Z';
    const proof = {
        type: "JsonWebKey2020",
        created: createdDate,
        jws: `${jwt_elts[0]}..${jwt_elts[2]}`,
        proofPurpose: "assertionMethod",
        verificationMethod: options.verificationMethod
    }
    if ("proof" in claim && Array.isArray(claim.proof)) {
        claim.proof.push(proof);
    } else {
        claim.proof = proof
    }

    return claim;
}

export async function generateVC(config, req, email) {
    let issuanceDate = (new Date()).toISOString();
    issuanceDate = issuanceDate.substr(0, issuanceDate.length - 5) + 'Z';
    let expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 3);
    expirationDate = expirationDate.toISOString();
    expirationDate = expirationDate.substr(0, expirationDate.length - 5) + 'Z';

    const controller = getController(req);
    
    let claim = {
        "@context": {
            "cred": "https://w3id.org/credentials/v1",
            "jws": "https://w3id.org/security/suites/jws-2020/v1"
        },
        "cred:id": req.get('host') + '/' + req.originalUrl,
        "cred:type": [
            "VerifiableCredential",
            "ParticipantCredential"
        ],
        "cred:issuer": controller,
        "cred:issuanceDate": issuanceDate,
        "cred:expirationDate": expirationDate,
        "cred:credentialSubject": {
            "@context": { "schema": "https://schema.org/" },
            "@type": "schema:ProgramMembership",
            "schema:id": `mailto:${email}`,
            "schema:programName": "Gaia-X AISBL"
        }
    }

    console.log("Sign the JSON-LD Document")
    {
        const signed_claim = await jws_sign({
            claim,
            privateKey: config.cert.domain.key,
            verificationMethod: controller + '#JsonWebKey2020-RSA'});
        console.log(signed_claim);
        // return signed_claim;
    }

    const privateKeyMultibase = convertToMultibase(config.cert.issuer.privateKey);
    const publicKeyMultibase = convertToMultibase(config.cert.issuer.publicKey);

    await verifyWithDer(publicKeyMultibase, privateKeyMultibase);
    await verifyWithJwk(publicKeyMultibase, privateKeyMultibase);
    {
        const keyPair = await Ed25519VerificationKey2020.from({
            type: 'Ed25519VerificationKey2020',
            controller,
            id: controller + '#Ed25519VerificationKey2020',
            publicKeyMultibase: publicKeyMultibase,
            privateKeyMultibase: privateKeyMultibase
        });
        const suite = new Ed25519Signature2020({ key: keyPair });
        const signed_claim = await jsigs.sign(claim, {
            suite: suite,
            purpose: new AssertionProofPurpose(),
            documentLoader: myDocumentLoader
        });
        console.log(signed_claim);
        return signed_claim;
    }
}

